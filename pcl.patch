diff --git a/CMakeLists.txt b/CMakeLists.txt
index aa59b7602..7cb09abaf 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -287,11 +287,17 @@ else()
 endif()
 
 # Threads (required)
-find_package(Threads REQUIRED)
+if(!QNX)
+  find_package(Threads REQUIRED)
+endif()
 
 # Eigen (required)
 find_package(Eigen 3.1 REQUIRED)
-include_directories(SYSTEM ${EIGEN_INCLUDE_DIRS})
+if (QNX)
+  include_directories(${EIGEN_INCLUDE_DIRS})
+else ()
+  include_directories(SYSTEM ${EIGEN_INCLUDE_DIRS})
+endif ()
 
 # FLANN (required)
 if(NOT PCL_SHARED_LIBS OR ((WIN32 AND NOT MINGW) AND NOT PCL_BUILD_WITH_FLANN_DYNAMIC_LINKING_WIN32))
diff --git a/PCLConfig.cmake.in b/PCLConfig.cmake.in
index 9f78fcfa0..cd456c0fe 100644
--- a/PCLConfig.cmake.in
+++ b/PCLConfig.cmake.in
@@ -424,6 +424,12 @@ elseif(EXISTS "${PCL_DIR}/include/pcl/pcl_config.h")
   set(PCL_CONF_INCLUDE_DIR "${PCL_DIR}/include") # for pcl_config.h
   set(PCL_LIBRARY_DIRS "${PCL_DIR}/@LIB_INSTALL_DIR@")
   set(PCL_SOURCES_TREE "@CMAKE_SOURCE_DIR@")
+elseif(EXISTS "${PCL_ROOT}/../usr/include/pcl-${PCL_VERSION_MAJOR}.${PCL_VERSION_MINOR}/pcl/pcl_config.h")
+  # Found PCLConfig.cmake in a build tree of PCL
+  # pcl_message("PCL found into a build tree.")
+  set(PCL_CONF_INCLUDE_DIR "${PCL_ROOT}/../usr/include/pcl-${PCL_VERSION_MAJOR}.${PCL_VERSION_MINOR}") # for pcl_config.h
+  set(PCL_LIBRARY_DIRS "${PCL_ROOT}/lib")
+  set(PCL_SOURCES_TREE "/root/AutowareAuto/qnx_deps/src/pcl-1.11.0")
 else()
   pcl_report_not_found("PCL can not be found on this machine")
 endif()
diff --git a/README.md b/README.md
index 26de07d43..379d0b21d 100644
--- a/README.md
+++ b/README.md
@@ -1,3 +1,8 @@
+## Original Source
+url: https://github.com/PointCloudLibrary/pcl.git  <br>
+commit: f9f214f34a38d5bb67441140703a681c5d299906 <br>
+tag: pcl-1.11.0 <br>
+
 # Point Cloud Library
 
 <p align="center"><img src="pcl.png" height="100"></p>
diff --git a/cmake/Modules/FindFLANN.cmake b/cmake/Modules/FindFLANN.cmake
index 782ee01c7..5c8a0d5be 100644
--- a/cmake/Modules/FindFLANN.cmake
+++ b/cmake/Modules/FindFLANN.cmake
@@ -36,6 +36,7 @@
 
 # Early return if FLANN target is already defined. This makes it safe to run
 # this script multiple times.
+
 if(TARGET FLANN::FLANN)
   return()
 endif()
@@ -93,21 +94,7 @@ endif()
 
 find_library(FLANN_LIBRARY
   NAMES
-    ${FLANN_RELEASE_NAME}
-  HINTS
-    ${PC_FLANN_LIBRARY_DIRS}
-    ${FLANN_ROOT}
-    $ENV{FLANN_ROOT}
-  PATHS
-    $ENV{PROGRAMFILES}/Flann
-    $ENV{PROGRAMW6432}/Flann
-  PATH_SUFFIXES
-    lib
-)
-
-find_library(FLANN_LIBRARY_DEBUG
-  NAMES
-    ${FLANN_DEBUG_NAME}
+    flann_cpp
   HINTS
     ${PC_FLANN_LIBRARY_DIRS}
     ${FLANN_ROOT}
diff --git a/cmake/pcl_targets.cmake b/cmake/pcl_targets.cmake
index 9dc695672..da86f35cb 100644
--- a/cmake/pcl_targets.cmake
+++ b/cmake/pcl_targets.cmake
@@ -223,7 +223,11 @@ function(PCL_ADD_LIBRARY _name)
   PCL_ADD_VERSION_INFO(${_name})
   target_compile_features(${_name} PUBLIC ${PCL_CXX_COMPILE_FEATURES})
   # must link explicitly against boost.
-  target_link_libraries(${_name} ${Boost_LIBRARIES} Threads::Threads)
+  if(QNX)
+    target_link_libraries(${_name} ${Boost_LIBRARIES} gomp )
+  else()
+    target_link_libraries(${_name} ${Boost_LIBRARIES} Threads::Threads)
+  endif()
   if(TARGET OpenMP::OpenMP_CXX)
     target_link_libraries(${_name} OpenMP::OpenMP_CXX)
   endif()
@@ -233,7 +237,7 @@ function(PCL_ADD_LIBRARY _name)
   endif()
 
   if(MINGW)
-    target_link_libraries(${_name} gomp)
+    target_link_libraries(${_name} )
   endif()
 
   if(MSVC)
@@ -306,8 +310,11 @@ function(PCL_ADD_EXECUTABLE _name)
   endif()
   PCL_ADD_VERSION_INFO(${_name})
   # must link explicitly against boost.
-  target_link_libraries(${_name} ${Boost_LIBRARIES} Threads::Threads)
-
+  if(QNX)
+    target_link_libraries(${_name} ${Boost_LIBRARIES} gomp )
+  else()
+    target_link_libraries(${_name} ${Boost_LIBRARIES} Threads::Threads)
+  endif()
   if(WIN32 AND MSVC)
     set_target_properties(${_name} PROPERTIES DEBUG_OUTPUT_NAME ${_name}${CMAKE_DEBUG_POSTFIX}
                                               RELEASE_OUTPUT_NAME ${_name}${CMAKE_RELEASE_POSTFIX})
@@ -384,8 +391,12 @@ macro(PCL_ADD_TEST _name _exename)
   #target_link_libraries(${_exename} ${GTEST_BOTH_LIBRARIES} ${PCL_ADD_TEST_LINK_WITH})
   target_link_libraries(${_exename} ${PCL_ADD_TEST_LINK_WITH} ${CLANG_LIBRARIES})
 
-  target_link_libraries(${_exename} Threads::Threads)
-
+  if(QNX)
+    target_link_libraries(${_exename} ${Boost_LIBRARIES} gomp )
+  else()
+    target_link_libraries(${_exename} Threads::Threads)
+  endif()
+  
   # must link explicitly against boost only on Windows
   target_link_libraries(${_exename} ${Boost_LIBRARIES})
 
diff --git a/io/include/pcl/io/low_level_io.h b/io/include/pcl/io/low_level_io.h
index 39ad6d246..07569522a 100644
--- a/io/include/pcl/io/low_level_io.h
+++ b/io/include/pcl/io/low_level_io.h
@@ -58,7 +58,7 @@ using ssize_t = SSIZE_T;
 # include <sys/mman.h>
 # include <sys/types.h>
 # include <sys/stat.h>
-# include <sys/fcntl.h>
+# include <fcntl.h>
 # include <cerrno>
 #endif
 #include <cstddef>
